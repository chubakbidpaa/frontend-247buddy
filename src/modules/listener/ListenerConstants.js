/* Authored by Chubak Bidpaa: chubakbidpaa@gmail.com - 2020 - Corona Times */

export const SET_LISTENER_DATA = "SET_LISTENER_DATA";
export const SET_STATUS = "SET_STATUS";
export const SET_AVATAR = "SET_AVATAR";
export const SET_CATEGORIES = "SET_CATEGORIES";